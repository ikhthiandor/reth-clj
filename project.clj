(defproject reth-clj "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [rethinkdb "0.9.39"]]
  :main ^:skip-aot reth-clj.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
